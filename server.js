'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const server = express();

// create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const { body, validationResult } = require('express-validator');
const { sanitizeBody } = require('express-validator');

server.set('view engine', 'ejs');

server.get('/', function(req, res){
    res.render('pages/indexv2');
});
server.get('/hugues-capet', function (req, res) {
    res.render('pages/hugues-capet');
});
server.get('/louisix', function (req, res) {
    res.render('pages/louisix');
});
server.get('/philippe-le-bel', function (req, res) {
    res.render('pages/philippe-le-bel');
});
server.get('/philippe-auguste', function (req, res) {
    res.render('pages/philippe-auguste');
});
server.get('/contact', function (req, res) {
    res.render('pages/contact');
});
server.post('/contact', urlencodedParser, [
    body('family_name').isAlpha(),
    body('first_name').isAlpha(),
    body('city').isAlpha(),
    body('e_mail').isEmail(),
], function (req, res) {
    console.log(req.body);

    const errors = validationResult(req);
    console.log(req.body);
    console.log('yeah');

    if (!errors.isEmpty()) {
        return res.status(422).jsonp(errors.array());
        //console.log(errors.array());
    } else {
        res.render('pages/contact');
        // res.send({});
        // appendData(req.body);
    }

    // res.render('pages/contact');
    appendData(req.body);
});

server.use(express.static(__dirname + '/public'));



function appendData(element) {
    let data = JSON.stringify(element, null, 2);
    fs.appendFile('result.json', "\n"+ data, function (erreur) {
        if (erreur) {
            console.log(erreur);
        };
    });
};



server.listen(8050);